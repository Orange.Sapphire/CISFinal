Rails.application.routes.draw do
  get 'sales/new'

  get    '/session/new',   to: 'session#new'
  post   '/session',   to: 'session#create'
  delete '/logout',  to: 'session#destroy'
  get   '/sales/shop', to: 'sales#new'
  post '/sales/dates', to: 'sales#total'
  get '/sales/dates.json', to: 'sales#total'
  get '/sales/dates', to: 'sales#total'
  root 'orders#index'
  resources :order_items do
    member do
      get :line_calc
    end
  end
  resources :menu_items
  resources :orders do
    member do
      get :price_calc
      get :line_calc
    end
  end
  resources :employees
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
