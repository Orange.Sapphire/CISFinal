class OrderItem < ApplicationRecord
  belongs_to :order
  belongs_to :menu_item

  after_save :decrement_inventory

  def decrement_inventory
    count = menu_item.inventory - quantity
    menu_item.update_inventory(count)
  end
  def line_total
    if menu_item.nil?
      return 0
    else
      menu_item.price * quantity
    end
  end
end
