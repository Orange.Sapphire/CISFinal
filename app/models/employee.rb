class Employee < ApplicationRecord
  has_many :orders

  def sales_range (start_date, end_date)
    emp_orders = Order.where(:created_at => start_date..end_date, :employee_id => id)
    emp_orders.map(&:subtotal).sum.round(2)
  end
end
