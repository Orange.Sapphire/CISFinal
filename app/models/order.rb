class Order < ApplicationRecord
  belongs_to :employee
  has_many :order_items
  has_many :menu_items, through: :order_items
  accepts_nested_attributes_for :order_items, allow_destroy: true

after_save :update_totals

  def update_totals
    sub = calc_sub
    update_columns(subtotal: sub)
    tx = calc_taxes
    tot = calc_total
    update_columns(tax: tx, total: tot)
  end

  def calc_sub
   order_items.map(&:line_total).sum.round(2)
  end

  def calc_taxes
    (subtotal * 0.1).round(2)
  end

  def calc_total
    (subtotal + calc_taxes).round(2)
    end

  def sales_range (start_date, end_date)
    sales = Order.where(:created_at => start_date..end_date)
    sales.map(&:sub_total).sum.round(2)
  end
end
