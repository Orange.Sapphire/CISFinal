module SalesHelper
  def sales_range (start_date, end_date)
    sales = Order.where(:created_at => start_date..end_date)
    sales.map(&:subtotal).sum.round(2)
  end
end
